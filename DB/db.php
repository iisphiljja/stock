<?php
	
	$GLOBALS['localhost'] = "127.0.0.1";
	$GLOBALS['$username'] = "homestead";
	$GLOBALS['$password'] = "secret";
	$GLOBALS['$dbname'] = "stock";


	function logInUser($username){
		
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = "SELECT * FROM User WHERE Name = '$username'";
			$result = $connect->query($sql);
		}
		
		$connect->close();
		
		return $result;
	}
	
	function logInSuperUser($name){
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = "SELECT * FROM SuperUser WHERE Name = '$name'";
			$result = $connect->query($sql);
		}
		$connect->close();
		
		return $result;
	}
	
	function createCompany($superuser_id, $name){
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		
		if($connect->connect_error) {
			print_r("error");
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = "INSERT INTO Company (SuperUser_FK, Name, CompanyLogo) VALUES ('$superuser_id', '$name', '')";
			$connect->query($sql);
			$companyID = $connect->insert_id;
		}
		$connect->close();
		
		return $companyID;
	}
	
	
	// *******************************   USER functions    *******************************   
	function addUser($company_FK, $name, $password, $fName, $lName, $status){
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		$userID = 0;
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = "SELECT user_ID FROM User WHERE Name = '$name'";
			$res = $connect->query($sql);

			$password = password_hash($password, PASSWORD_DEFAULT);
			
			if($res->num_rows == 0){
				$sql = "INSERT INTO User (Company_FK, Name, Password, FName, LName, Status) VALUES ('$company_FK', '$name', '$password', '$fName', '$lName', '$status')";
				$connect->query($sql);
				
				$userID = $connect->insert_id;
			}
		}
		
		$connect->close();
		return $userID; 
	}
	
	function addUserFunction($user_FK, $userRole_FK){
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		$found = false;
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			
			$sql = "INSERT INTO Function (User_FK, UserRole_FK) VALUES ('$user_FK', '$userRole_FK')";
			$found = $connect->query($sql) === TRUE ? true : false;
		}
		
		$connect->close();
		
		return $found; 
	}
	
	function readUser($user_ID){
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		$res = null;
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = ($user_ID == 0) ? "SELECT * FROM User" :
				"SELECT * FROM User WHERE User_ID = '$user_ID'";
			$res = $connect->query($sql);
		}
		
		$connect->close();
		return $res;
	}
	
	function updateUser($user_ID, $firstName, $lastName, $status){
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		$found = false;
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = "SELECT user_ID FROM User WHERE User_ID = '$user_ID'";
			$res = $connect->query($sql);
			
			
			if($res->num_rows > 0){
				$sql = "UPDATE User SET FName = '$firstName', LName = '$lastName', Status = '$status' WHERE User_ID = '$user_ID'";
				$res1 = $connect->query($sql);
				
				
				print_r($res1);
			
				$found = true;
			}
		}
		
		$connect->close();
		return $found;
	}
	
	
	// *******************************   PRODUCT functions    ******************************* 
	function addProduct($SKU, $productName, $product_image_url, $brandName, $categoryName,$quantity, $rate, $productStatus){
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		$SKU1 = "";
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = "SELECT SKU FROM product WHERE SKU = '$SKU'";
			$res = $connect->query($sql);
			
			if($res->num_rows == 0){
				$sql = "INSERT INTO product (SKU, product_name, product_image, brand_id, categories_id, quantity, rate, active, status) 
					VALUES ('$SKU', '$productName', '$product_image_url', '$brandName', '$categoryName', '$quantity', '$rate', '$productStatus','1')";
				$connect->query($sql);
				
				$SKU1 = $SKU;
			}
		}
		
		$connect->close();
		return $SKU1; 
	}
	
	function readProduct($SKU){
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		$res = null;
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = ($user_ID == 0) ? "SELECT * FROM Item" :
				"SELECT * FROM User WHERE SKU = '$SKU'";
			$res = $connect->query($sql);
		}
		
		$connect->close();
		return $res;
	}
	
	function updateProduct($SKU, $productName, $brandName, $categoryName,$quantity, $rate, $productStatus){
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		$res = null;
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = "UPDATE product SET product_name = '$productName', brand_id = '$brandName', categories_id = '$categoryName', quantity = '$quantity', 
			rate = '$rate', active = '$productStatus', status = 1 WHERE SKU = $SKU";

			$res = $connect->query($sql);
		}
		
		$connect->close();
		return $res;
	}

	// *******************************   USER ROLE    ******************************* 
	function getUserRole($user_ID){
		
		$connect = new mysqli($GLOBALS['localhost'], $GLOBALS['$username'], $GLOBALS['$password'], $GLOBALS['$dbname']);
		
		if($connect->connect_error) {
			die("Connection Failed : " . $connect->connect_error);
		} else {
			$sql = "SELECT * FROM Function where User_FK = '$user_ID'";

			$result = $connect->query($sql);

			while( $row = $result->fetch_assoc() ) {
				$userRole_FK = $row['UserRole_FK'];
				$sql2 = "SELECT * FROM UserRole where UserRole_ID = '$userRole_FK'";

				$result2 = $connect->query($sql2);
				$record[] = $result2->fetch_assoc();
			}

		}
		
		$connect->close();
		return $record;
	}
?>