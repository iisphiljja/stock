<?php 	

require_once 'core.php';

$companyId = $_SESSION['companyId'];
$sql = "SELECT * FROM User where Company_FK = ".$companyId;

$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) { 

	// $row = $result->fetch_array();
	$active = ""; 

	while($row = $result->fetch_array()) {

	 	$user_id = $row[0];
	 	// active 
	 	if($row[6] == 1) {
	 		// activate member
	 		$active = "<label class='label label-success'>Available</label>";
	 	} else {
	 		// deactivate member
	 		$active = "<label class='label label-danger'>Not Available</label>";
	 	} // /else

	 	$button = '<!-- Single button -->
		<div class="btn-group">
		  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    Action <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu">
		    <li><a type="button" data-toggle="modal" id="editEmployeeModalBtn" data-target="#editEmployeeModal" onclick="editEmployee('.$user_id.')"> <i class="glyphicon glyphicon-edit"></i> Edit</a></li>
		    <li><a type="button" data-toggle="modal" data-target="#removeEmployeeModal" id="removeEmployeeModalBtn" onclick="removeEmployee('.$user_id.')"> <i class="glyphicon glyphicon-trash"></i> Remove</a></li>       
		  </ul>
		</div>';

		unset($user_role);
		$sql2 = "SELECT * FROM Function f
					LEFT JOIN UserRole uR ON f.userRole_fk = uR.userRole_id
					WHERE f.user_fk = ".$user_id;

		$result2 = $connect->query($sql2);
		
		while($row2 = $result2->fetch_assoc()) {
			$user_role[] = $row2['Description'];
		}

		$userRole = implode(",",$user_role);

	 	$output['data'][] = array( 		
			// FNAME
			$row[4],
	 		// LNAME
	 		$row[5], 
	 		// USERNAME
	 		$row[2],
	 		// USER ROLE
	 		$userRole,
	 		// active
	 		$active,
	 		// button
	 		$button 		
	 	); 	
	} // /while 

}// if num_rows

$connect->close();

echo json_encode($output);