<?php require_once 'php_action/db_connect.php' ?>
<?php require_once 'includes/header.php'; ?>

<div class="row">
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboard.php">Home</a></li>		  
		  <li class="active">Employee</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Manage Employee</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">

				<div class="remove-messages"></div>

				<div class="div-action pull pull-right" style="padding-bottom:20px;">
					<button class="btn btn-default button1" data-toggle="modal" id="addEmployeeModalBtn" data-target="#addEmployeeModal"> <i class="glyphicon glyphicon-plus-sign"></i> Add Employee </button>
				</div> <!-- /div-action -->				
				
				<table class="table" id="manageEmployeeTable">
					<thead>
						<tr>
							<th>First Name</th>					
							<th>Last Name</th>
							<th>User Name</th>		
							<th>User Role</th>
							<th>Status</th>
							<th style="width:15%;">Options</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->

			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->


<!-- add employee -->
<div class="modal fade" id="addEmployeeModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

    	<form class="form-horizontal" id="submitEmployeeForm" action="php_action/createEmployee.php" method="POST" enctype="multipart/form-data">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><i class="fa fa-plus"></i> Add Employee</h4>
			</div>

			<div class="modal-body" style="max-height:450px; overflow:auto;">

				<div id="add-employee-messages"></div>	
				<div class="form-group" style="display: none">
					<label for="companyId" class="col-sm-3 control-label">Company Id: </label>
					<label class="col-sm-1 control-label">: </label>
						<div class="col-sm-8">
						  <input type="text" class="form-control" id="companyId" placeholder="Company Id" name="companyId" value="<?php echo $_SESSION['companyId']?>" autocomplete="off">
						</div>
				</div> <!-- /form-group-->

				<div class="form-group">
					<label for="firstName" class="col-sm-3 control-label">First Name : </label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="firstName" placeholder="First Name" name="firstName" autocomplete="off" required>
					</div>
				</div> <!-- /form-group-->

				<div class="form-group">
					<label for="lastName" class="col-sm-3 control-label">Last Name : </label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="lastName" placeholder="Last Name" name="lastName" autocomplete="off" required>
					</div>
				</div> <!-- /form-group-->

				<div class="form-group">
					<label for="userName" class="col-sm-3 control-label">User Name : </label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="userName" placeholder="User Name" name="userName" autocomplete="off" required>
					</div>
				</div> <!-- /form-group-->

				<div class="form-group" style="display: none">
					<label for="password" class="col-sm-3 control-label">Password: </label>
					<label class="col-sm-1 control-label">: </label>
						<div class="col-sm-8">
						  <input type="text" class="form-control" id="password" placeholder="Password" name="password" value="password" autocomplete="off">
						</div>
				</div> <!-- /form-group-->

				<div class="form-group">
					<label for="userRole" class="col-sm-3 control-label">User Role : </label>

					<div class="col-sm-8">
						<div id="userRole"></div>
						<div class="checkbox">
						  <label><input type="checkbox" name="userRole[]" value="3">Sales</label>
						</div>
						<div class="checkbox">
						  <label><input type="checkbox" name="userRole[]" value="4">Operation</label>
						</div>
						<div class="checkbox">
						  <label><input type="checkbox" name="userRole[]" value="5">Accounting</label>
						</div>
						<div class="checkbox">
						  <label><input type="checkbox" name="userRole[]" value="6">Marketing</label>
						</div>
					</div>
				</div> <!-- /form-group-->
			
			</div> <!-- /modal-body -->
	      
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Close</button>
				<button type="submit" class="btn btn-primary" id="createEmployeeBtn" data-loading-text="Loading..." autocomplete="off"> <i class="glyphicon glyphicon-ok-sign"></i> Save Changes</button>
			</div> <!-- /modal-footer -->	      
     	</form> <!-- /.form -->	     
    </div> <!-- /modal-content -->    
  </div> <!-- /modal-dailog -->
</div> 
<!-- /add categories -->


<!-- edit Employee -->
<div class="modal fade" id="editEmployeeModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	    	
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Employee</h4>
	      </div>
	      <div class="modal-body" style="max-height:450px; overflow:auto;">

	      	<div class="div-loading">
	      		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
						<span class="sr-only">Loading...</span>
	      	</div>

	      	<div class="div-result">

				    <!-- employee image -->
				    <div role="tabpanel" class="tab-pane active" id="employeeInfo">
				    	<form class="form-horizontal" id="editEmployeeForm" action="php_action/editEmployee.php" method="POST">					
							<br />

							<div id="edit-employee-messages"></div>

							<div class="form-group" style="display: none">
								<label for="companyId" class="col-sm-3 control-label">Company Id: </label>
								<label class="col-sm-1 control-label">: </label>
									<div class="col-sm-8">
									  <input type="text" class="form-control" id="companyId" placeholder="Company Id" name="companyId" value="<?php echo $_SESSION['companyId']?>" autocomplete="off">
									</div>
							</div> <!-- /form-group-->

							<div class="form-group">
								<label for="firstName" class="col-sm-3 control-label">First Name : </label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="editfirstName" placeholder="First Name" name="firstName" autocomplete="off" required>
								</div>
							</div> <!-- /form-group-->

							<div class="form-group">
								<label for="lastName" class="col-sm-3 control-label">Last Name : </label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="editlastName" placeholder="Last Name" name="lastName" autocomplete="off" required>
								</div>
							</div> <!-- /form-group-->

							<div class="form-group">
								<label for="userName" class="col-sm-3 control-label">User Name : </label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="edituserName" placeholder="User Name" name="userName" autocomplete="off" required>
								</div>
							</div> <!-- /form-group-->

							<div class="form-group" style="display: none">
								<label for="password" class="col-sm-3 control-label">Password: </label>
								<label class="col-sm-1 control-label">: </label>
									<div class="col-sm-8">
									  <input type="text" class="form-control" id="password" placeholder="Password" name="password" value="password" autocomplete="off">
									</div>
							</div> <!-- /form-group-->

							<div class="form-group">
								<label for="userRole" class="col-sm-3 control-label">User Role : </label>

								<div class="col-sm-8">
									<div id="edituserRole"></div>
									<div class="checkbox">
									  <label><input type="checkbox" name="edituserRole[]" value="3">Sales</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" name="edituserRole[]" value="4">Operation</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" name="edituserRole[]" value="5">Accounting</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" name="edituserRole[]" value="6">Marketing</label>
									</div>
								</div>
							</div> <!-- /form-group-->	         	        

							<div class="modal-footer editEmployeeFooter">
								<button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Close</button>
								<button type="submit" class="btn btn-success" id="editEmployeeBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Save Changes</button>
							</div> <!-- /modal-footer -->				     
						</form> <!-- /.form -->				     	
				    </div>    
				    <!-- /employee info -->

				</div>
	      	
	      </div> <!-- /modal-body -->
	      	      
     	
    </div>
    <!-- /modal-content -->
  </div>
  <!-- /modal-dailog -->
</div>
<!-- /categories brand -->

<!-- categories brand -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeEmployeeModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-trash"></i> Remove Employee</h4>
      </div>
      <div class="modal-body">

      	<div class="removeEmployeeMessages"></div>

        <p>Do you really want to remove ?</p>
      </div>
      <div class="modal-footer removeEmployeeFooter">
        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Close</button>
        <button type="button" class="btn btn-primary" id="removeEmployeeBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /categories brand -->


<script src="custom/js/employee.js"></script>

<?php require_once 'includes/footer.php'; ?>