<?php 	

include('../DB/db.php');

$valid['success'] = array('success' => false, 'messages' => array());

if($_POST) {
	
	$SKU 			= $_POST['SKU'];
	$productName 	= $_POST['editProductName']; 
	$quantity 		= $_POST['editQuantity'];
	$rate 			= $_POST['editRate'];
	$brandName 		= $_POST['editBrandName'];
	$categoryName 	= $_POST['editCategoryName'];
	$productStatus 	= $_POST['editProductStatus'];

	if(updateProduct($SKU, $productName, $brandName, $categoryName,$quantity, $rate, $productStatus) === TRUE) {
		$valid['success'] = true;
		$valid['messages'] = "Successfully Update";	
	} else {
		$valid['success'] = false;
		$valid['messages'] = "Error while updating product info";
	}

} // /$_POST
	 
echo json_encode($valid);
 
