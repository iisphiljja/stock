<?php 	
include('../DB/db.php');
require_once 'core2.php';


$valid['success'] = array('success' => false, 'messages' => array());

if($_POST) {	
	$name = $_POST['companyName'];
	$superuser_id = $_SESSION['SuperUserId'];

	$companyID = createCompany($superuser_id, $name);
	
	if($companyID > 0) {
	 	$valid['success'] = true;
		$valid['messages'] = "Successfully Added";	
	} else {
	 	$valid['success'] = false;
	 	$valid['messages'] = "Error while adding the members";
	}

	echo json_encode($valid);
 
} // /if $_POST