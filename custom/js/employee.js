var manageEmployeeTable;

$(document).ready(function() {
	// top nav bar 
	$('#navEmployee').addClass('active');
	// manage employee data table
	manageEmployeeTable = $('#manageEmployeeTable').DataTable({
		'ajax': 'php_action/fetchEmployee.php',
		'order': []
	});

	// add employee modal btn clicked
	$("#addEmployeeModalBtn").unbind('click').bind('click', function() {
		// // Employee form reset
		$("#submitEmployeeForm")[0].reset();		

		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');

		// submit employee form
		$("#submitEmployeeForm").unbind('submit').bind('submit', function() {

			// form validation
			//var employeeImage = $("#employeeImage").val();
			var firstName 	= $("#firstName").val();
			var lastName 	= $("#lastName").val();
			var userName 	= $("#userName").val();
			var password 	= $("#password").val();
			// var userRole 	= $("input[type='checkbox']").val();

			// alert(userRole);
	
			
			if(firstName == "") {
				$("#firstName").after('<p class="text-danger">Employee First Name field is required</p>');
				$('#firstName').closest('.form-group').addClass('has-error');
			} else {
				// remov error text field
				$("#firstName").find('.text-danger').remove();
				// success out for form 
				$("#firstName").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(lastName == "") {
				$("#lastName").after('<p class="text-danger">Employee Last Name field is required</p>');
				$('#lastName').closest('.form-group').addClass('has-error');
			} else {
				// remov error text field
				$("#lastName").find('.text-danger').remove();
				// success out for form 
				$("#lastName").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(userName == "") {
				$("#userName").after('<p class="text-danger">Employee User Name field is required</p>');
				$('#userName').closest('.form-group').addClass('has-error');
			} else {
				// remov error text field
				$("#userName").find('.text-danger').remove();
				// success out for form 
				$("#userName").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if ($('input[name="userRole[]"]:checked').length > 0) {
			    // one or more checked
			    var userRole = true;
			} else {
			    $("#userRole").html('<p class="text-danger">Employee User Role field is required</p>');
				$('#userRole').closest('.form-group').addClass('has-error');
				var userRole = false;
			}

		

			//if(employeeImage && employeeName && quantity && rate && brandName && categoryName && employeeStatus) {
			if(firstName && lastName && userName && userRole) {
				// submit loading button
				$("#createEmployeeBtn").button('loading');

				var form = $(this);
				var formData = new FormData(this);
	
				$.ajax({
					url : form.attr('action'),
					type: form.attr('method'),
					data: formData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					success:function(response) {
						$("#createEmployeeBtn").button('reset');
						
												
						$('#add-employee-messages').html('<div class="alert alert-success">'+
							'<button type="button" class="close" data-dismiss="alert">&times;</button>'+
							'<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
							'</div>');
							
						if(response.success == true) {
							$("#submitEmployeeForm")[0].reset();
							
							
							$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
						
							// remove the mesages
							$(".alert-success").delay(500).show(10, function() {
								$(this).delay(3000).hide(10, function() {
									$(this).remove();
								});
							}); // /.alert

							// reload the manage student table
							manageEmployeeTable.ajax.reload(null, true);

							// remove text-error 
							$(".text-danger").remove();
							// remove from-group error
							$(".form-group").removeClass('has-error').removeClass('has-success');
						}
					}, // /success function
					error: function(response){
						console.log("Error here >>>> " + JSON.stringify(response));
						$("#createEmployeeBtn").button('reset');
						
						$('#add-employee-messages').html('<div class="alert alert-success">'+
							'<button type="button" class="close" data-dismiss="alert">&times;</button>'+
							'<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
							'</div>');
					}
				}); // /ajax function
			}	 // /if validation is ok 					

			return false;
		}); // /submit employee form

	}); // /add employee modal btn clicked
	

	// remove employee 	

}); // document.ready fucntion

function editEmployee(employeeId = null) {

	if(employeeId) {
		$("#employeeId").remove();		
		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');
		// modal spinner
		$('.div-loading').removeClass('div-hide');
		// modal div
		$('.div-result').addClass('div-hide');

		$.ajax({
			url: 'php_action/fetchSelectedEmployee.php',
			type: 'post',
			data: {user_id: employeeId},
			dataType: 'json',
			success:function(response) {		
				// console.log(response.FName);
				// modal spinner
				$('.div-loading').addClass('div-hide');
				// modal div
				$('.div-result').removeClass('div-hide');				

				// employee name
				$("#editfirstName").val(response.FName);
				// quantity
				$("#editlastName").val(response.LName);
				// rate
				$("#edituserName").val(response.Name);

				// update the employee data function
				$("#editEmployeeForm").unbind('submit').bind('submit', function() {

					// form validation
					//var employeeImage = $("#editEmployeeImage").val();
					var editfirstName = $("#editfirstName").val();
					var editlastName = $("#editlastName").val();
					var edituserName = $("#edituserName").val();
							

					if(employeeName == "") {
						$("#editEmployeeName").after('<p class="text-danger">Employee Name field is required</p>');
						$('#editEmployeeName').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editEmployeeName").find('.text-danger').remove();
						// success out for form 
						$("#editEmployeeName").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(quantity == "") {
						$("#editQuantity").after('<p class="text-danger">Quantity field is required</p>');
						$('#editQuantity').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editQuantity").find('.text-danger').remove();
						// success out for form 
						$("#editQuantity").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(rate == "") {
						$("#editRate").after('<p class="text-danger">Rate field is required</p>');
						$('#editRate').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editRate").find('.text-danger').remove();
						// success out for form 
						$("#editRate").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(brandName == "") {
						$("#editBrandName").after('<p class="text-danger">Brand Name field is required</p>');
						$('#editBrandName').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editBrandName").find('.text-danger').remove();
						// success out for form 
						$("#editBrandName").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(categoryName == "") {
						$("#editCategoryName").after('<p class="text-danger">Category Name field is required</p>');
						$('#editCategoryName').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editCategoryName").find('.text-danger').remove();
						// success out for form 
						$("#editCategoryName").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(employeeStatus == "") {
						$("#editEmployeeStatus").after('<p class="text-danger">Employee Status field is required</p>');
						$('#editEmployeeStatus').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editEmployeeStatus").find('.text-danger').remove();
						// success out for form 
						$("#editEmployeeStatus").closest('.form-group').addClass('has-success');	  	
					}	// /else					

					if(employeeName && quantity && rate && brandName && categoryName && employeeStatus) {
						// submit loading button
						$("#editEmployeeBtn").button('loading');

						var form = $(this);;
						var formData = new FormData(this);

						$.ajax({
							url : form.attr('action'),
							type: form.attr('method'),
							data: formData,
							dataType: 'json',
							cache: false,
							contentType: false,
							processData: false,
							success:function(response) {
								console.log("here >>>> " + response);
								if(response.success == true) {
									// submit loading button
									$("#editEmployeeBtn").button('reset');																		

									$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																			
									// shows a successful message after operation
									$('#edit-employee-messages').html('<div class="alert alert-success">'+
										'<button type="button" class="close" data-dismiss="alert">&times;</button>'+
										'<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
									'</div>');

									// remove the mesages
									$(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert

									// reload the manage student table
									manageEmployeeTable.ajax.reload(null, true);

									// remove text-error 
									$(".text-danger").remove();
									// remove from-group error
									$(".form-group").removeClass('has-error').removeClass('has-success');

								} // /if response.success
								
							} // /success function
						}); // /ajax function
					}	 // /if validation is ok 					

					return false;
				}); // update the employee data function
			} // /success function
		}); // /ajax to fetch employee image

				
	} else {
		alert('error please refresh the page');
	}
} // /edit employee function

// remove employee 
function removeEmployee(employeeId = null) {
	if(employeeId) {
		// remove employee button clicked
		$("#removeEmployeeBtn").unbind('click').bind('click', function() {
			// loading remove button
			$("#removeEmployeeBtn").button('loading');
			$.ajax({
				url: 'php_action/removeEmployee.php',
				type: 'post',
				data: {employeeId: employeeId},
				dataType: 'json',
				success:function(response) {
					// loading remove button
					$("#removeEmployeeBtn").button('reset');
					if(response.success == true) {
						// remove employee modal
						$("#removeEmployeeModal").modal('hide');

						// update the employee table
						manageEmployeeTable.ajax.reload(null, false);

						// remove success messages
						$(".remove-messages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					} else {

						// remove success messages
						$(".removeEmployeeMessages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert

					} // /error
				} // /success function
			}); // /ajax fucntion to remove the employee
			return false;
		}); // /remove employee btn clicked
	} // /if employeeid
} // /remove employee function

function clearForm(oForm) {
	// var frm_elements = oForm.elements;									
	// console.log(frm_elements);
	// 	for(i=0;i<frm_elements.length;i++) {
	// 		field_type = frm_elements[i].type.toLowerCase();									
	// 		switch (field_type) {
	// 	    case "text":
	// 	    case "password":
	// 	    case "textarea":
	// 	    case "hidden":
	// 	    case "select-one":	    
	// 	      frm_elements[i].value = "";
	// 	      break;
	// 	    case "radio":
	// 	    case "checkbox":	    
	// 	      if (frm_elements[i].checked)
	// 	      {
	// 	          frm_elements[i].checked = false;
	// 	      }
	// 	      break;
	// 	    case "file": 
	// 	    	if(frm_elements[i].options) {
	// 	    		frm_elements[i].options= false;
	// 	    	}
	// 	    default:
	// 	        break;
	//     } // /switch
	// 	} // for
}