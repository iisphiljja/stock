<?php 	
include('../DB/db.php');
require_once 'core2.php';


$valid['success'] = array('success' => false, 'messages' => array());
$valid2['success'] = array('success' => false, 'messages' => array());

if($_POST) {	
	// print_r($_POST);
	$company_fk 	= $_POST['company_id'];
	$firstName 	= $_POST['firstName'];
	$lastName 	= $_POST['lastName'];
	$userName 	= $_POST['userName'];
	$password 	= password_hash($_POST['password'], PASSWORD_DEFAULT);
	$status 	= 1; 

	$user_ID = addUser($company_fk, $userName, $password, $firstName, $lastName, $status);
		
	if($user_ID != 0) {
		$valid['success'] = true;
		$valid['messages'] = "Successfully Added";	
		
		$res = addUserFunction($user_ID,1);			// TODO: loop to the number of function Rules
		
		if($res) {
			$valid['success'] = true;
			$valid['messages'] = "Successfully added Employee funtion role.";
		} else {
			$valid['success'] = true;
			$valid['messages'] = "Error in adding Employee funtion role.";
		}
	} else {
		$valid['success'] = false;
		$valid['messages'] = "Employee name already exist.";
	}

	echo json_encode($valid);
 
} // /if $_POST