<?php 	

include('../DB/db.php');

$valid['success'] = array('success' => false, 'messages' => array());

if($_POST) {	
	// print_r($_POST);
	$company_FK 	= $_POST['companyId'];
	$firstName 		= $_POST['firstName'];
	$lastName 		= $_POST['lastName'];
	$userName 		= $_POST['userName'];
	$password 		= $_POST['password'];
	$userRole 		= $_POST['userRole'];
	$status 		= 1;

	$res_id = addUser($company_FK, $userName, $password, $firstName, $lastName, $status);

	foreach( $userRole as $k => $v ) {
		$results = addUserFunction($res_id, $v);
	}
	
	if($results > 0) {
		$valid['success'] = true;
		$valid['messages'] = "Successfully Added";	
	} else {
		$valid['success'] = false;
		$valid['messages'] = "Error while adding the members";
	}


} // /if $_POST

echo json_encode($valid);